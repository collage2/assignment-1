import { useLoaderData } from 'react-router-dom';
import classes from './productPage.module.css';
import products from '../data/products';

export const loader = ({ params }) => {
    const product = products.find(product => product.id == params.productId);
    return { product };
}

const ProductPage = () => {
    const { product } = useLoaderData();
    return (
        <div className={classes.product}>
            <div className={classes.details}>
                <div className={classes.title}>Product Name:</div>
                <div className={classes.subtitle}>{product.name}</div>
                <div className={classes.title}>Price:</div>
                <div className={classes.subtitle}>{product.price}₪</div>
                <div className={classes.title}>Store:</div>
                <div className={classes.subtitle}>{product.store}</div>
                <div>
                    <div className={classes.title}>Description:</div>
                    <div>{product.description}</div>
                </div>
            </div>
            <img src={product.img} alt={product.name} className={classes.img} />
        </div>
    );
}

export default ProductPage;