import milkUrl from '../assets/milk.png';
import coffeUrl from '../assets/coffe.png';
import cornUrl from '../assets/corn.png';

export default [
    {
        id: 1,
        name: 'Milk',
        price: 15,
        store: 'Tnuva',
        description: 'Regular cow milk',
        img: milkUrl
    },
    {
        id: 2,
        name: 'Coffe',
        price: 20,
        store: 'Elit',
        description: 'The best coffe',
        img: coffeUrl
    },
    {
        id: 3,
        name: 'Corn',
        price: 5,
        store: 'Corni',
        description: 'All you need is a good corn',
        img: cornUrl
    },
]