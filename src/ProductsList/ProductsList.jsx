import { useLoaderData, Link } from 'react-router-dom';
import productsList from '../data/products';
import Product from './SingleProduct/SingleProduct';
import classes from './productsList.module.css';

export const loader = () => {
    return { productsList };
}

const ProductsList = () => {
    const { productsList } = useLoaderData();
    return (
        <>
            <h2>Products List:</h2>
            <div className={classes.products}>
                {
                    productsList.map(product =>
                        <Link to={`products/${product.id}`} key={product.id} className={classes.link}>
                            <Product {...product} />
                        </Link>
                    )
                }
            </div>
        </>
    );
}

export default ProductsList;