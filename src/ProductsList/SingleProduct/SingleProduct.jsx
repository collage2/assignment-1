import classes from './singleProduct.module.css';

const SingleProduct = ({ name, price, img, }) => {

    return (
        <div className={classes.product}>
            <div className={classes.details}>
                <div className={classes.name}>{name}</div>
                <div className={classes.price}>{price}₪</div>
            </div>
            <img src={img} alt={name} className={classes.img} />
        </div>
    );
}

export default SingleProduct;