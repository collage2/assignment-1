import { Outlet } from "react-router-dom";
import classes from './app.module.css';

const App = () => {
  return (
    <div className={classes.root}>
      <div className={classes.container}>
        <Outlet />
      </div>
    </div>
  )
}

export default App
