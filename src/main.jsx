import React from "react";
import ReactDOM from "react-dom/client";
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import App from "./App";
import ProductsList, { loader as productsListLoader } from "./ProductsList/ProductsList";
import ProductPage, { loader as productLoader } from './ProductPage/ProductPage';
import ErrorPage from './ErrorPage/ErrorPage';
import "./index.css";

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "",
        element: <ProductsList />,
        loader: productsListLoader,
      },
      {
        path: "products/:productId",
        element: <ProductPage />,
        loader: productLoader,
      },
    ],
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);